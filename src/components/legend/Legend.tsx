import { FC } from "react";
import classes from "./Legend.module.scss";

interface ILegendProps {
  icon: string;
  title: string;
}

const Legend: FC<ILegendProps> = ({ icon, title }) => {
  return (
    <div className={classes.numberEntryFinalDecisionLegend}>
      <div className={classes.numberEntryFinalDecisionLegendIconContainer}>
        <img
          src={icon}
          alt="legendImg"
          className={classes.numberEntryFinalDecisionLegendIcon}
        />
      </div>

      <h2 className={classes.numberEntryFinalDecisionLegendTitle}>{title}</h2>
    </div>
  );
};

export default Legend;
