import React, { FC, useState, useEffect, useRef } from "react";
import classes from "./ImageCarousel.module.scss";
import { ICarouselImageData } from "../../types/imageCarouselTypes";
import { useHistory } from "react-router-dom";
import { Path } from "../../types/routePathTypes";

interface IImageCarouselProps {
  images: ICarouselImageData[];
}

const ImageCarousel: FC<IImageCarouselProps> = ({ images }) => {
  const [currentImageIndex, setImageIndex] = useState<number>(0);
  const history = useHistory();

  const componentnRef = useRef<HTMLDivElement>(null);

  const isLastImageInCarousel = images.length - 1 === currentImageIndex;
  const isFirstImageInCarousel = currentImageIndex === 0;

  const isActiveImage = (index: number): boolean => {
    return currentImageIndex === index;
  };

  const indicatorClasses: string[] = [classes.circle, classes.activeCircle];

  const onArrowBackClickHandler = (): void => {
    setImageIndex(currentImageIndex - 1);
  };

  const onArrowForwardClickHandler = (): void => {
    setImageIndex(currentImageIndex + 1);
  };

  const onImageCancelBtnClickHandler = () =>
    history.push(window.location.pathname + Path.Exit);

  const OnImageCarouselContainerKeDown = (
    e: React.KeyboardEvent<HTMLDivElement>
  ) => {
    const isKeyArrowLeft = e.key === "ArrowLeft";
    const isKeyArrowRight = e.key === "ArrowRight";

    if (isKeyArrowLeft) {
      if (!isFirstImageInCarousel) {
        onArrowBackClickHandler();
      }
    }
    if (isKeyArrowRight) {
      if (!isLastImageInCarousel) {
        onArrowForwardClickHandler();
      }
    }
  };

  useEffect(() => {
    componentnRef.current?.focus();
  }, []);

  return (
    <div
      id="imageCarouselContainer"
      className={classes.imageCarouselContainer}
      ref={componentnRef}
      tabIndex={-1}
      onKeyDown={OnImageCarouselContainerKeDown}
    >
      <div className={classes.imageCarouselImagesContainer}>
        <div className={classes.arrowBackContainer}>
          {isFirstImageInCarousel || (
            <i
              className={["fas fa-chevron-circle-left", classes.arrowBack].join(
                " "
              )}
              onClick={onArrowBackClickHandler}
            ></i>
          )}
        </div>
        <div className={classes.imageContentContainer}>
          <div className={classes.imageContent}>
            <img
              src={images[currentImageIndex].image}
              className={classes.imageOfCarousel}
              alt="img"
            />
            <div className={classes.imageIndicator}>
              {images.map((i, index) => (
                <span
                  key={i.image}
                  className={
                    isActiveImage(index)
                      ? indicatorClasses.join(" ")
                      : classes.circle
                  }
                ></span>
              ))}
            </div>
            <div className={classes.imageTextContainer}>
              <h1 className={classes.imageTitle}>
                {images[currentImageIndex].title}
              </h1>
              <p className={classes.imageDescription}>
                {images[currentImageIndex].description}
              </p>
            </div>
            <div className={classes.imageCancelBtnContainer}>
              <button
                className={classes.imageCancelBtn}
                onClick={onImageCancelBtnClickHandler}
              >
                отмена
              </button>
            </div>
          </div>
        </div>
        <div className={classes.arrowForwardContainer}>
          {isLastImageInCarousel || (
            <i
              className={[
                "fas fa-chevron-circle-right",
                classes.arrowBack,
              ].join(" ")}
              onClick={onArrowForwardClickHandler}
            ></i>
          )}
        </div>
      </div>
    </div>
  );
};

export default ImageCarousel;
