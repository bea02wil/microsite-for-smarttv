import React, { MutableRefObject, useEffect, useRef } from "react";
import classes from "./PhoneNumberPanel.module.scss";
import appClasses from "../../App.module.scss";
import InputMask, { ReactInputMask } from "react-input-mask";
import PhoneKeyboard from "../phoneKeyboard/PhoneKeyboard";
import { phoneKeyItemData } from "../../data/phoneKeyItemData";
import { useActions } from "../../hooks/useActions";
import { useTypedSelector } from "../../hooks/useTypedSelector";
import { PhoneKeyValue } from "../../types/phoneKeyItemTypes";

interface IPhoneNumberPanelProps {
  productName: string;
}

const PhoneNumberPanel = React.forwardRef<
  HTMLDivElement,
  IPhoneNumberPanelProps
>(({ productName }, ref) => {
  const { phoneNumber, isAgree } = useTypedSelector(
    (state) => state.phoneNumberPanel
  );
  const { setAgreement, setKeyFocus, setPhoneNumberValid } = useActions();

  const licenseAgreementCheckboxRef = useRef<HTMLInputElement>(null);
  const termsOfUseRef = useRef<HTMLAnchorElement>(null);
  const inputMaskRef = useRef<ReactInputMask>(null);

  const onCheckboxInputChangeHandler = (
    e: React.ChangeEvent<HTMLInputElement>
  ) => {
    const value = e.target.checked;

    setKeyFocus("");
    setAgreement(value);
  };

  const onCheckboxMouseOverHandler = () => {
    if (
      !licenseAgreementCheckboxRef.current?.classList.contains(
        appClasses.stopCheckboxAnim
      )
    ) {
      licenseAgreementCheckboxRef.current?.classList.add(
        appClasses.stopCheckboxAnim
      );
    }
  };

  const onCheckboxKeyDownHandler = (
    e: React.KeyboardEvent<HTMLInputElement>
  ) => {
    const isKeyArrowUp = e.key === "ArrowUp";
    const isKeyArrowDown = e.key === "ArrowDown";
    const isKeyEnter = e.key === "Enter";

    if (isKeyArrowUp) {
      setKeyFocus(PhoneKeyValue.Zero);
    } else if (isKeyArrowDown) {
      termsOfUseRef.current?.focus();
    } else if (isKeyEnter) {
      setKeyFocus("");
      setAgreement(!isAgree);
    }
  };

  const onLinkKeyDownHandler = (e: React.KeyboardEvent<HTMLAnchorElement>) => {
    const isKeyArrowUp = e.key === "ArrowUp";
    const isKeyArrowRight = e.key === "ArrowRight";

    if (isKeyArrowUp) {
      licenseAgreementCheckboxRef.current?.focus();
    }
    if (isKeyArrowRight) {
      const mutableRef = ref as MutableRefObject<HTMLDivElement>;
      const personalOfferBtn = mutableRef.current?.querySelector(
        "#personalOfferBtn"
      ) as HTMLElement;

      personalOfferBtn?.classList.add(appClasses.phoneNumberCompleted);
      personalOfferBtn?.focus();
    }
  };

  useEffect(() => {
    const maxPhoneNumberCount = 12;
    const phoneNumberCountWithoutLastDigit = 11;

    const isPhoneNumberFull = phoneNumber.length === maxPhoneNumberCount;
    const isPhoneNumberWithoutLastDigit =
      phoneNumber.length === phoneNumberCountWithoutLastDigit;

    const mutableRef = ref as MutableRefObject<HTMLDivElement>;
    const personalOfferBtn = mutableRef.current?.querySelector(
      "#personalOfferBtn"
    ) as HTMLElement;

    if (isPhoneNumberFull) {
      personalOfferBtn?.classList.add(appClasses.phoneNumberCompleted);

      setPhoneNumberValid(phoneNumber);
    } else if (isPhoneNumberWithoutLastDigit) {
      //через useRef<ReactInputMask>(null) inputMask->inputMaskRef не содержит style, classList и прочее,
      //т.к. не явл HTMLElement. При касте к HTMLElement | null возвращается null
      const phoneNumberInput = mutableRef.current?.querySelector(
        "#phoneNumberInput"
      ) as HTMLElement;

      phoneNumberInput!.style.color = "#5a5757";

      personalOfferBtn?.classList.remove(appClasses.phoneNumberCompleted);
    }

    //eslint-disable-next-line
  }, [phoneNumber]);

  return (
    <div className={classes.phoneNumberPanelContainer}>
      <div className={classes.phoneNumberPanel}>
        <div className={classes.phoneTitleContainer}>
          <p className={classes.phoneTitle}>
            Пожалуйста, введите номер телефона. Мы свяжемся с Вами в ближайшее
            время.
          </p>
        </div>
        <div className={classes.phoneInputContainer}>
          <div className={classes.phoneInputSubContainer}>
            <InputMask
              id="phoneNumberInput"
              mask="+7 ( 999 ) 999 - 99 - 99"
              value={phoneNumber}
              alwaysShowMask
              className={classes.phoneInput}
              readOnly
              ref={inputMaskRef}
            />
          </div>
        </div>
        <PhoneKeyboard phoneKeyItemData={phoneKeyItemData} ref={ref} />
        <div className={classes.phoneNumberPanelLicenseAgreement}>
          <div className={classes.phoneNumberPanelCheckboxInputContainer}>
            <input
              id="licenseAgreement"
              type="checkbox"
              className={classes.phoneNumberPanelCheckboxInput}
              checked={isAgree}
              onChange={onCheckboxInputChangeHandler}
              onMouseOver={onCheckboxMouseOverHandler}
              onKeyDown={onCheckboxKeyDownHandler}
              ref={licenseAgreementCheckboxRef}
            />
          </div>
          <p className={classes.phoneNumberPanelLicenseAgreementText}>
            Я согласен(-на) с тем, что оставленная мной информация может быть
            использована компанией ООО --
            <span className={classes.phoneNumberPanelProductName}>
              {productName}
            </span>
            --.{" "}
            <a
              id="termsOfUse"
              href="#"
              className={classes.phoneNumberPanelTermsOfUse}
              onKeyDown={onLinkKeyDownHandler}
              ref={termsOfUseRef}
            >
              Условия использования Персональных данных
            </a>
          </p>
        </div>
      </div>
    </div>
  );
});

export default PhoneNumberPanel;
