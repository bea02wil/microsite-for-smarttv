import classes from "./ExitWindow.module.scss";
import { backdrop } from "../../functions/backDrop";
import { useHistory } from "react-router-dom";
import { FC, useEffect } from "react";

interface IExitWindowProps {
  agreementBtnClickRouteTo: string;
  cancelBtnClickRouteTo: string;
}

const ExitWindow: FC<IExitWindowProps> = ({
  agreementBtnClickRouteTo,
  cancelBtnClickRouteTo,
}) => {
  useEffect(() => {
    backdrop.add(classes.darkBackdrop);

    return () => {
      backdrop.remove(classes.darkBackdrop);
    };
  }, []);

  const history = useHistory();

  const redirectTo = (path: string): void => {
    history.push(path);
  };

  const onAgreementBtnClickHandler = () => {
    backdrop.remove(classes.darkBackdrop);

    redirectTo(agreementBtnClickRouteTo);
  };

  const onCancelBtnClickHandler = () => {
    backdrop.remove(classes.darkBackdrop);

    redirectTo(cancelBtnClickRouteTo);
  };

  return (
    <div className={classes.exitWindow}>
      <div className={classes.exitWindowHeader}>
        <p className={classes.exitWindowTitle}>вы уверены, что хотите выйти?</p>
      </div>
      <div className={classes.exitWindowFooter}>
        <div className={classes.exitWindowButtons}>
          <div
            className={classes.exitWindowAgreementBtn}
            onClick={onAgreementBtnClickHandler}
          >
            да
          </div>
          <div
            className={classes.exitWindowCancelBtn}
            onClick={onCancelBtnClickHandler}
          >
            нет
          </div>
        </div>
      </div>
    </div>
  );
};

export default ExitWindow;
