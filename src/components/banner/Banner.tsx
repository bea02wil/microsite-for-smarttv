import { FC, useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import classes from "./Banner.module.scss";
import { Path } from "../../types/routePathTypes";

interface IBannerProps {
  acceptTitle: string;
  buttonKeyName: string;
  bodyText: string;
  bodyImg: string;
  appearanceTime: number;
}

const Banner: FC<IBannerProps> = ({
  acceptTitle,
  buttonKeyName,
  bodyText,
  bodyImg,
  appearanceTime,
}) => {
  const history = useHistory();

  const onClickHandler = () => {
    history.push(Path.ProductInfoScreen);
  };

  const [bannerClasses, setBannerClasses] = useState<string[]>([
    classes.banner,
  ]);

  useEffect(() => {
    //Нестрогие appearanceTime секунд.
    setTimeout(() => {
      setBannerClasses([classes.banner, classes.bannerTranslate]);
    }, appearanceTime * 1000);

    //eslint-disable-next-line
  }, []);

  return (
    <div className={bannerClasses.join(" ")} onClick={onClickHandler}>
      <div className={classes.bannerAcceptContainer}>
        <div className={classes.bannerAccept}>
          <div className={classes.bannerAcceptTitle}>{acceptTitle}</div>
          <div className={classes.circle}>
            <div className={classes.bannerAcceptBtnKey}>{buttonKeyName}</div>
          </div>
        </div>
      </div>
      <div className={classes.bannerBodyContainer}>
        <div className={classes.bannerBody}>
          <div className={classes.bannerBodyText}>{bodyText}</div>
          <div className={classes.bannerBodyImgContainer}>
            <img src={bodyImg} className={classes.bannerBodyImg} alt="img" />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Banner;
