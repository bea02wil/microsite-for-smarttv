import { FC } from "react";
import { IProductInfoProps } from "../../types/aboutProductTypes";
import classes from "./AboutProduct.module.scss";
import { useHistory } from "react-router-dom";
import { Path } from "../../types/routePathTypes";

const AboutProduct: FC<IProductInfoProps> = (props) => {
  const history = useHistory();

  const {
    features,
    featuresTitle,
    logo,
    productImage,
    productProgramDescription,
    productTitle,
  } = props;

  const onProductAcceptBtnClickHandler = () => {
    history.push(Path.NumberEntryScreen);
  };

  return (
    <div className={classes.aboutProductContainer}>
      <div className={classes.productHeader}>
        <h1 className={classes.productTitle}>{productTitle}</h1>
        <div className={classes.aboutProductLogoContainer}>
          <img src={logo} alt="logo" className={classes.productLogo} />
        </div>
      </div>
      <div className={classes.productBody}>
        <div className={classes.featuresСontainer}>
          <h2 className={classes.featuresTitle}>{featuresTitle}</h2>
          <ul className={classes.features}>
            {features.map((feature) => (
              <li key={feature} className={classes.feature}>
                {feature}
              </li>
            ))}
          </ul>
        </div>
        <div className={classes.productImageContainer}>
          <img
            src={productImage}
            alt="productImage"
            className={classes.productImage}
          />
        </div>
        <div className={classes.productProgramDescriptionContainer}>
          <p className={classes.productProgramDescription}>
            {productProgramDescription}
          </p>
        </div>
      </div>
      <div className={classes.productFooter}>
        <div
          className={classes.productAcceptBtnContainer}
          onClick={onProductAcceptBtnClickHandler}
        >
          <div className={classes.productAcceptBtnLeftPart}>ок</div>
          <div className={classes.productAcceptBtnRightPart}>
            получить персональное предложение
          </div>
        </div>
      </div>
    </div>
  );
};

export default AboutProduct;
