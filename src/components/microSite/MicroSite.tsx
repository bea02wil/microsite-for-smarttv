import { FC } from "react";
import { IProductCompany } from "../../types/aboutProductTypes";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import classes from "./MicroSite.module.scss";
import { Path } from "../../types/routePathTypes";
import PromoVideoWithBannerScreen from "../../pages/promoVideoWithBannerScreen/PromoVideoWithBannerScreen";
import ProductInfoScreen from "../../pages/productInfoScreen/ProductInfoScreen";
import ExitWindow from "../exitWindow/ExitWindow";
import NumberEntryScreen from "../../pages/numberEntryScreen/NumberEntryScreen";
import ContactAlertWindow from "../contactAlertWindow/ContactAlertWindow";

interface IMicroSiteProps {
  product: IProductCompany;
}

const MicroSite: FC<IMicroSiteProps> = ({ product }) => {
  return (
    <div className={classes.microSite} id="microSite">
      <Router>
        <Route
          exact
          path={Path.Home}
          component={() => (
            <PromoVideoWithBannerScreen
              videoTitle={product.company.promoVideo.title}
              src={product.company.promoVideo.src}
              bannerImg={product.company.banner.img}
            />
          )}
        ></Route>
        <Switch>
          <Route
            path={Path.ProductInfoScreenAlternativeProduct}
            component={() => (
              <ProductInfoScreen
                images={product.company.model.secondModel.carouselImages!}
                aboutProductData={product.company.model.secondModel}
                productlogo={product.company.model.firstModel.logo}
              />
            )}
          ></Route>
          <Route
            path={Path.ProductInfoScreen}
            component={() => (
              <ProductInfoScreen
                images={product.company.model.firstModel.carouselImages!}
                aboutProductData={product.company.model.firstModel}
                productlogo={product.company.model.firstModel.logo}
              />
            )}
          ></Route>
        </Switch>
        <Route
          path={Path.ProductInfoScreenExit}
          component={() => (
            <ExitWindow
              agreementBtnClickRouteTo={Path.Home}
              cancelBtnClickRouteTo={Path.ProductInfoScreen}
            />
          )}
        ></Route>
        <Route
          path={Path.NumberEntryScreen}
          component={() => (
            <NumberEntryScreen
              backgroundImage={product.company.backgroundImg}
              productName={product.company.name}
              productLogo={product.company.model.firstModel.logo}
            />
          )}
        ></Route>
        <Route
          path={Path.NumberEntryScreenContactAlert}
          component={() => (
            <ContactAlertWindow productName={product.company.brandName} />
          )}
        ></Route>
        <Route
          path={Path.NumberEntryScreenExit}
          component={() => (
            <ExitWindow
              agreementBtnClickRouteTo={
                Path.ProductInfoScreenAlternativeProduct
              }
              cancelBtnClickRouteTo={Path.NumberEntryScreen}
            />
          )}
        ></Route>
        <Route
          path={Path.ProductInfoScreenAlternativeProductExit}
          component={() => (
            <ExitWindow
              agreementBtnClickRouteTo={Path.Home}
              cancelBtnClickRouteTo={Path.ProductInfoScreenAlternativeProduct}
            />
          )}
        ></Route>
      </Router>
    </div>
  );
};

export default MicroSite;
