import React, { useRef, useEffect, MutableRefObject } from "react";
import { IPhoneKeyItemProps } from "../../types/phoneKeyItemTypes";
import classes from "./PhoneKeyItem.module.scss";
import appClasses from "../../App.module.scss";
import { PhoneKeyItemClickAction } from "../../types/phoneKeyItemTypes";
import { useActions } from "../../hooks/useActions";
import { useTypedSelector } from "../../hooks/useTypedSelector";

const PhoneKeyItem = React.forwardRef<HTMLDivElement, IPhoneKeyItemProps>(
  (
    {
      keyValue,
      itemClass = classes.phoneKeyItem,
      clickAction = PhoneKeyItemClickAction.SetDigit,
      xIndex,
      yIndex,
    },
    ref
  ) => {
    const { phoneNumber, keyAnimation, keyFocus } = useTypedSelector(
      (state) => state.phoneNumberPanel
    );

    const componentRef = useRef<HTMLDivElement>(null);
    const {
      setPhoneNumber,
      deletePhoneNumberDigit,
      setKeyAnimation,
      setKeyFocus,
    } = useActions();

    useEffect(() => {
      if (keyFocus === keyValue) {
        componentRef.current?.focus();
      }
    });

    const onClickHandler = () => {
      setKeyAnimation(keyValue as string);
      setKeyFocus("");

      if (clickAction === PhoneKeyItemClickAction.SetDigit) {
        setPhoneNumber(phoneNumber + keyValue);
      } else {
        deletePhoneNumberDigit();
      }
    };

    const onKeyDownHandler = (e: React.KeyboardEvent<HTMLDivElement>) => {
      const isKeyEnter = e.key === "Enter";

      if (isKeyEnter) {
        if (componentRef.current === document.activeElement) {
          onClickHandler();
        }
      }
    };

    const onMouseOverHandler = () => {
      const mutableRef = ref as MutableRefObject<HTMLDivElement>;
      const personalOfferBtn =
        mutableRef.current?.querySelector("#personalOfferBtn");

      if (
        personalOfferBtn?.classList.contains(appClasses.phoneNumberCompleted)
      ) {
        personalOfferBtn?.classList.remove(appClasses.phoneNumberCompleted);
      }

      componentRef.current?.focus();
    };

    return (
      <div
        className={`${itemClass} ${
          keyAnimation === keyValue ? classes.phoneKeyItemScaleAnim : ""
        } `}
        onClick={onClickHandler}
        onAnimationEnd={() => setKeyAnimation("")}
        ref={componentRef}
        onMouseOver={onMouseOverHandler}
        onKeyDown={onKeyDownHandler}
        tabIndex={0}
        data-xindex={xIndex}
        data-yindex={yIndex}
      >
        {keyValue}
      </div>
    );
  }
);

export default PhoneKeyItem;
