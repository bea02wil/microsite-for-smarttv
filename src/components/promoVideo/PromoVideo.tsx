import { FC } from "react";
import classes from "./PromoVideo.module.scss";

interface IPromoVideoProps {
  width: string;
  height: string;
  title: string;
  src: string;
}

const PromoVideo: FC<IPromoVideoProps> = ({ width, height, title, src }) => {
  return (
    <div className={classes.promoVideo}>
      <iframe
        width={width}
        height={height}
        title={title}
        src={src}
        allow="autoplay"
      ></iframe>
    </div>
  );
};

export default PromoVideo;
