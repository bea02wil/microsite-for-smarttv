import React, { FC } from "react";
import classes from "./NumberEntryFinalDecisionBtn.module.scss";

interface INumberEntryFinalDecisionBtnProps {
  id?: string;
  btnClass: string;
  name: string;
  onClick: () => void;
  onKeyDown: (e: React.KeyboardEvent<HTMLButtonElement>) => void;
}

const NumberEntryFinalDecisionBtn: FC<INumberEntryFinalDecisionBtnProps> = ({
  id,
  btnClass,
  name,
  onKeyDown,
  onClick,
}) => {
  return (
    <button
      id={id}
      className={`${classes.numberEntryFinalDecisionBtn} ${btnClass}`}
      onClick={onClick}
      onKeyDown={onKeyDown}
    >
      {name}
    </button>
  );
};

export default NumberEntryFinalDecisionBtn;
