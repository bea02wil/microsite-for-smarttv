import classes from "./PhoneKeyboard.module.scss";
import appClasses from "../../App.module.scss";
import PhoneKeyItem from "../phoneKeyItem/PhoneKeyItem";
import { IPhoneKeyItemProps } from "../../types/phoneKeyItemTypes";
import React, { MutableRefObject } from "react";
import { useActions } from "../../hooks/useActions";
import { useTypedSelector } from "../../hooks/useTypedSelector";

interface IPhoneKeybordProps {
  phoneKeyItemData: IPhoneKeyItemProps[];
}

enum Direction {
  Left = "Left",
  Right = "Right",
  Up = "Up",
  Down = "Down",
}

interface INextElementToNavigate {
  nextLeftElementXIndex: number;
  nextLeftElementYIndex: number;
  elementDirection: string;
}

type NextPossibleElement = INextElementToNavigate | undefined;

const PhoneKeyboard = React.forwardRef<HTMLDivElement, IPhoneKeybordProps>(
  ({ phoneKeyItemData }, ref) => {
    const {
      setKeyFocus,
      setPhoneNumber,
      setKeyAnimation,
      deletePhoneNumberDigit,
    } = useActions();
    const { phoneNumber } = useTypedSelector((state) => state.phoneNumberPanel);

    const getNextElementToNavigate = (
      direction: Direction
    ): NextPossibleElement => {
      const activeElement = document.activeElement as HTMLElement;

      const activeElementXIndex = activeElement?.dataset.xindex;
      const activeElementYIndex = activeElement?.dataset.yindex;

      if (activeElementXIndex && activeElementYIndex) {
        let nextLeftElementXIndex = 0;
        let nextLeftElementYIndex = 0;
        let elementDirection = "";

        switch (direction) {
          case Direction.Left:
            nextLeftElementXIndex = +activeElementXIndex;
            nextLeftElementYIndex = +activeElementYIndex - 1;
            elementDirection = Direction.Left;
            break;
          case Direction.Right:
            nextLeftElementXIndex = +activeElementXIndex;
            nextLeftElementYIndex = +activeElementYIndex + 1;
            elementDirection = Direction.Right;
            break;
          case Direction.Up:
            nextLeftElementXIndex = +activeElementXIndex - 1;
            nextLeftElementYIndex = +activeElementYIndex;
            elementDirection = Direction.Up;
            break;
          case Direction.Down:
            nextLeftElementXIndex = +activeElementXIndex + 1;
            nextLeftElementYIndex = +activeElementYIndex;
            elementDirection = Direction.Down;
            break;
        }

        return {
          nextLeftElementXIndex,
          nextLeftElementYIndex,
          elementDirection,
        };
      }
    };

    const focusElement = (element: NextPossibleElement): void => {
      const mutableRef = ref as MutableRefObject<HTMLDivElement>;
      const nextActiveElement = mutableRef.current?.querySelector(
        `[data-xindex='${element?.nextLeftElementXIndex}'][data-yindex='${element?.nextLeftElementYIndex}']`
      ) as HTMLElement;

      if (nextActiveElement) {
        nextActiveElement?.focus();
      } else {
        if (element?.elementDirection === Direction.Right) {
          const personalOfferBtn = mutableRef.current?.querySelector(
            "#personalOfferBtn"
          ) as HTMLElement;

          personalOfferBtn?.classList.add(appClasses.phoneNumberCompleted);
          personalOfferBtn?.focus();
          return;
        }

        if (element?.elementDirection === Direction.Down) {
          const licenseAgreementCheckbox = mutableRef.current?.querySelector(
            "#licenseAgreement"
          ) as HTMLElement;

          licenseAgreementCheckbox?.classList.add(appClasses.stopCheckboxAnim);
          licenseAgreementCheckbox?.focus();
          return;
        }
      }
    };

    const onKeyDownHandler = (e: React.KeyboardEvent<HTMLDivElement>) => {
      const isKeyDigit = +e.key >= 0 && +e.key <= 9 && e.code !== "Space";
      const isKeyBackspace = e.key === "Backspace";
      const isKeyArrowLeft = e.key === "ArrowLeft";
      const isKeyArrowRight = e.key === "ArrowRight";
      const isKeyArrowUp = e.key === "ArrowUp";
      const isKeyArrowDown = e.key === "ArrowDown";

      if (isKeyDigit) {
        setPhoneNumber(phoneNumber + e.key);
        setKeyAnimation(e.key);
        setKeyFocus(e.key);
      } else if (isKeyBackspace) {
        deletePhoneNumberDigit();
      } else if (isKeyArrowLeft) {
        const nextLeftElement = getNextElementToNavigate(Direction.Left);

        focusElement(nextLeftElement);
      } else if (isKeyArrowRight) {
        const nextRightElement = getNextElementToNavigate(Direction.Right);

        focusElement(nextRightElement);
      } else if (isKeyArrowUp) {
        const nextUpElement = getNextElementToNavigate(Direction.Up);

        focusElement(nextUpElement);
      } else if (isKeyArrowDown) {
        const nextDownElement = getNextElementToNavigate(Direction.Down);

        focusElement(nextDownElement);
      }
    };
    return (
      <div className={classes.phoneKeyboard} onKeyDown={onKeyDownHandler}>
        {phoneKeyItemData.map((item, index) => (
          <PhoneKeyItem
            key={index}
            keyValue={item.keyValue}
            itemClass={item.itemClass}
            clickAction={item.clickAction}
            xIndex={item.xIndex}
            yIndex={item.yIndex}
            ref={ref}
          />
        ))}
      </div>
    );
  }
);

export default PhoneKeyboard;
