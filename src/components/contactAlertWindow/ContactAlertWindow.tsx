import { FC, useEffect } from "react";
import classes from "./ContactAlertWindow.module.scss";
import { Path } from "../../types/routePathTypes";
import { useHistory } from "react-router-dom";
import { backdrop } from "../../functions/backDrop";

interface IContactAlertWindowProps {
  productName: string;
}

const ContactAlertWindow: FC<IContactAlertWindowProps> = ({ productName }) => {
  useEffect(() => {
    backdrop.add(classes.darkBackdrop);

    return () => {
      backdrop.remove(classes.darkBackdrop);
    };
  }, []);

  const history = useHistory();

  const onClickHandler = () => {
    backdrop.remove(classes.darkBackdrop);

    history.push(Path.Home);
  };

  return (
    <div className={classes.contactAlertWindow}>
      <div className={classes.contactAlertWindowHeader}>
        <p className={classes.contactAlertWindowHeaderTitle}>
          благодарим за интерес к бренду {productName}
        </p>
      </div>
      <div className={classes.contactAlertWindowMain}>
        <p className={classes.contactAlertWindowMainTitle}>
          мы свяжемся с Вами в ближайшее время
        </p>
      </div>
      <div className={classes.contactAlertWindowFooter}>
        <button
          className={classes.contactAlertWindowBtn}
          onClick={onClickHandler}
        >
          ок
        </button>
      </div>
    </div>
  );
};

export default ContactAlertWindow;
