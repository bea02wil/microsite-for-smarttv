import React, { MutableRefObject } from "react";
import classes from "./NumberEntryFinalDecision.module.scss";
import appClasses from "../../App.module.scss";
import { PhoneKeyValue } from "../../types/phoneKeyItemTypes";
import NumberEntryFinalDecisionBtn from "../finalEntryDecisionBtn/NumberEntryFinalDecisionBtn";
import { useHistory } from "react-router-dom";
import { useTypedSelector } from "../../hooks/useTypedSelector";
import { useActions } from "../../hooks/useActions";
import { Path } from "../../types/routePathTypes";
import Legend from "../legend/Legend";
import SetOf09Numbers from "../../data/img/setOf0-9numbers.svg";
import MoveCursor from "../../data/img/move.svg";
import Ok from "../../data/img/ok.svg";

interface INumberEntryFinalDecisionProps {
  logo: string;
}

const NumberEntryFinalDecision = React.forwardRef<
  HTMLDivElement,
  INumberEntryFinalDecisionProps
>(({ logo }, ref) => {
  const { phoneNumber, isAgree, isPhoneNumberValid } = useTypedSelector(
    (state) => state.phoneNumberPanel
  );
  const { setKeyFocus } = useActions();

  const history = useHistory();

  const onClickPersonalOfferBtnHandler = () => {
    const maxPhoneNumberCount = 12;
    const isPhoneNumberFull = phoneNumber.length === maxPhoneNumberCount;

    const mutableRef = ref as MutableRefObject<HTMLDivElement>;

    if (!isPhoneNumberFull) {
      setKeyFocus(PhoneKeyValue.Five);
    } else if (isPhoneNumberFull && !isAgree) {
      const licenseAgreementCheckbox = mutableRef.current?.querySelector(
        "#licenseAgreement"
      ) as HTMLElement;

      if (
        licenseAgreementCheckbox?.classList.contains(
          appClasses.stopCheckboxAnim
        )
      ) {
        licenseAgreementCheckbox?.classList.remove(appClasses.stopCheckboxAnim);
      }

      licenseAgreementCheckbox?.focus();
    } else {
      const phoneNumberInput = mutableRef.current?.querySelector(
        "#phoneNumberInput"
      ) as HTMLElement;

      if (isPhoneNumberValid) {
        //В этой строке берем phoneNumber и отправляем куда-то: почта/сервер...
        phoneNumberInput!.style.color = "#5a5757";
        history.push(Path.NumberEntryScreenContactAlert);
      } else {
        phoneNumberInput!.style.color = "red";
      }
    }
  };

  const onKeyDownPersonalOfferBtnHandler = (
    e: React.KeyboardEvent<HTMLButtonElement>
  ) => {
    const isKeyArrowLeft = e.key === "ArrowLeft";
    const isKeyArrowDown = e.key === "ArrowDown";
    const isKeyEnter = e.key === "Enter";

    const mutableRef = ref as MutableRefObject<HTMLDivElement>;

    if (isKeyArrowLeft) {
      const personalOfferBtn =
        mutableRef.current?.querySelector("#personalOfferBtn");

      personalOfferBtn?.classList.remove(appClasses.phoneNumberCompleted);
      setKeyFocus(PhoneKeyValue.Five);
    } else if (isKeyArrowDown) {
      const cancelFinalDecisionBtn = mutableRef.current?.querySelector(
        "#cancelFinalDecisionBtn"
      ) as HTMLElement;
      const personalOfferBtn =
        mutableRef.current?.querySelector("#personalOfferBtn");

      personalOfferBtn?.classList.remove(appClasses.phoneNumberCompleted);
      cancelFinalDecisionBtn?.focus();
    } else if (isKeyEnter) {
      onClickPersonalOfferBtnHandler();
    }
  };

  const onClickCancelBtnHandler = () => {
    history.push(Path.NumberEntryScreenExit);
  };

  const onKeyDownCancelBtnHandler = (
    e: React.KeyboardEvent<HTMLButtonElement>
  ) => {
    const isKeyArrowLeft = e.key === "ArrowLeft";
    const isKeyArrowUp = e.key === "ArrowUp";
    const isKeyEnter = e.key === "Enter";

    const mutableRef = ref as MutableRefObject<HTMLDivElement>;

    if (isKeyArrowLeft) {
      setKeyFocus(PhoneKeyValue.Five);
    } else if (isKeyArrowUp) {
      const personalOfferBtn = mutableRef.current?.querySelector(
        "#personalOfferBtn"
      ) as HTMLElement;

      personalOfferBtn?.classList.add(appClasses.phoneNumberCompleted);
      personalOfferBtn?.focus();
    } else if (isKeyEnter) {
      onClickCancelBtnHandler();
    }
  };

  return (
    <div className={classes.numberEntryFinalDecisionContainer}>
      <div className={classes.numberEntryFinalDecision}>
        <div className={classes.numberEntryLogoContainer}>
          <img src={logo} alt="logo" className={classes.productLogo} />
        </div>
        <div className={classes.numberEntryDecisionBtnsContainer}>
          <NumberEntryFinalDecisionBtn
            id="personalOfferBtn"
            btnClass={classes.numberEntryPersonalOfferBtn}
            name="получить персональное предложение"
            onClick={onClickPersonalOfferBtnHandler}
            onKeyDown={onKeyDownPersonalOfferBtnHandler}
          />
          <NumberEntryFinalDecisionBtn
            id="cancelFinalDecisionBtn"
            btnClass={classes.numberEntryCancelBtn}
            name="отмена"
            onClick={onClickCancelBtnHandler}
            onKeyDown={onKeyDownCancelBtnHandler}
          />
        </div>
        <div className={classes.numberEntryFinalDecisionLegendContainer}>
          <Legend
            icon={SetOf09Numbers}
            title={
              "введите номер при помощи цифр на пульте или стрелок и клавиши ОК"
            }
          />
          <Legend icon={MoveCursor} title={"перемещение курсора"} />
          <Legend icon={Ok} title={"ОК - подтверждение выбора"} />
        </div>
      </div>
    </div>
  );
});

export default NumberEntryFinalDecision;
