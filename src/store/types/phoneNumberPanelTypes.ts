export interface IPhoneNumberPanelState {
  phoneNumber: string;
  isAgree: boolean;
  keyAnimation: string;
  keyFocus: string;
  isPhoneNumberValid: boolean;
}

export enum PhoneNumberPanelActionTypes {
  SET_PHONE_NUMBER = "SET_PHONE_NUMBER",
  SET_AGREEMENT = "SET_AGREEMENT",
  SET_KEY_ANIMATION = "SET_KEY_ANIMATION",
  SET_KEY_FOCUS = "SET_KEY_FOCUS",
  SET_PHONE_NUMBER_VALID = "SET_PHONE_NUMBER_VALID",
  DELETE_PHONE_NUMBER_DIGIT = "DELETE_PHONE_NUMBER_DIGIT",
}

export interface IPhoneInfoApi {
  valid: boolean;
}

interface ISetPhoneNumber {
  type: PhoneNumberPanelActionTypes.SET_PHONE_NUMBER;
  payload: string;
}

interface IDeletePhoneNumberDigit {
  type: PhoneNumberPanelActionTypes.DELETE_PHONE_NUMBER_DIGIT;
}

interface ISetAgreement {
  type: PhoneNumberPanelActionTypes.SET_AGREEMENT;
  payload: boolean;
}

interface ISetKeyAnimation {
  type: PhoneNumberPanelActionTypes.SET_KEY_ANIMATION;
  payload: string;
}

interface ISetKeyFocus {
  type: PhoneNumberPanelActionTypes.SET_KEY_FOCUS;
  payload: string;
}

interface ISetPhoneNumberValid {
  type: PhoneNumberPanelActionTypes.SET_PHONE_NUMBER_VALID;
  payload: boolean;
}

export type PhoneNumberPanelAction =
  | ISetPhoneNumber
  | IDeletePhoneNumberDigit
  | ISetAgreement
  | ISetKeyAnimation
  | ISetKeyFocus
  | ISetPhoneNumberValid;
