import {
  IPhoneNumberPanelState,
  PhoneNumberPanelAction,
  PhoneNumberPanelActionTypes,
} from "../types/phoneNumberPanelTypes";
import { PhoneKeyValue } from "../../types/phoneKeyItemTypes";

const initialState: IPhoneNumberPanelState = {
  phoneNumber: "+7",
  isAgree: true,
  keyAnimation: "",
  keyFocus: PhoneKeyValue.Five,
  isPhoneNumberValid: false,
};

const initialReducerPhoneNumberCount = initialState.phoneNumber.length;

const getMaxPhoneDigitInputLength = (
  phoneDigitLengthWithoutMask: number
): number => {
  const maxPhoneDigitInputLength =
    initialReducerPhoneNumberCount + phoneDigitLengthWithoutMask;

  return maxPhoneDigitInputLength;
};

const getPhoneDeletedNumberByOneDigit = (phoneNumber: string): string => {
  const deletedPhoneNumberByOneDigit = phoneNumber.substring(
    0,
    phoneNumber.length - 1
  );

  return deletedPhoneNumberByOneDigit;
};

export const phoneNumberPanelReducer = (
  state = initialState,
  action: PhoneNumberPanelAction
): IPhoneNumberPanelState => {
  switch (action.type) {
    case PhoneNumberPanelActionTypes.SET_PHONE_NUMBER:
      const maxPhoneDigitInputLength = getMaxPhoneDigitInputLength(10);

      return {
        ...state,
        phoneNumber:
          state.phoneNumber.length < maxPhoneDigitInputLength
            ? action.payload
            : state.phoneNumber,
      };
    case PhoneNumberPanelActionTypes.DELETE_PHONE_NUMBER_DIGIT:
      const deletedPhoneNumberByOneDigit = getPhoneDeletedNumberByOneDigit(
        state.phoneNumber
      );

      return {
        ...state,
        phoneNumber:
          state.phoneNumber.length > initialReducerPhoneNumberCount
            ? deletedPhoneNumberByOneDigit
            : state.phoneNumber,
      };
    case PhoneNumberPanelActionTypes.SET_AGREEMENT:
      return {
        ...state,
        isAgree: action.payload,
      };
    case PhoneNumberPanelActionTypes.SET_KEY_ANIMATION:
      return {
        ...state,
        keyAnimation: action.payload,
      };
    case PhoneNumberPanelActionTypes.SET_KEY_FOCUS:
      return {
        ...state,
        keyFocus: action.payload,
      };
    case PhoneNumberPanelActionTypes.SET_PHONE_NUMBER_VALID:
      return {
        ...state,
        isPhoneNumberValid: action.payload,
      };
    default:
      return state;
  }
};
