import { combineReducers } from "redux";
import { phoneNumberPanelReducer } from "./phoneNumberPanelReducer";

export const rootReducer = combineReducers({
  phoneNumberPanel: phoneNumberPanelReducer,
});

export type RootReducer = ReturnType<typeof rootReducer>;
