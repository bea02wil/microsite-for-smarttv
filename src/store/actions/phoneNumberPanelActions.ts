import {
  PhoneNumberPanelAction,
  PhoneNumberPanelActionTypes,
  IPhoneInfoApi,
} from "../types/phoneNumberPanelTypes";
import { Dispatch } from "redux";
import axios from "axios";

export const setPhoneNumber = (phoneNumber: string): PhoneNumberPanelAction => {
  return {
    type: PhoneNumberPanelActionTypes.SET_PHONE_NUMBER,
    payload: phoneNumber,
  };
};

export const deletePhoneNumberDigit = (): PhoneNumberPanelAction => {
  return {
    type: PhoneNumberPanelActionTypes.DELETE_PHONE_NUMBER_DIGIT,
  };
};

export const setAgreement = (agreement: boolean): PhoneNumberPanelAction => {
  return {
    type: PhoneNumberPanelActionTypes.SET_AGREEMENT,
    payload: agreement,
  };
};

export const setKeyAnimation = (
  keyAnimation: string
): PhoneNumberPanelAction => {
  return {
    type: PhoneNumberPanelActionTypes.SET_KEY_ANIMATION,
    payload: keyAnimation,
  };
};

export const setKeyFocus = (keyValue: string): PhoneNumberPanelAction => {
  return {
    type: PhoneNumberPanelActionTypes.SET_KEY_FOCUS,
    payload: keyValue,
  };
};

export const setPhoneNumberValid =
  (phoneNumber: string) =>
  async (dispatch: Dispatch<PhoneNumberPanelAction>) => {
    try {
      const phoneInfo = await axios.get<IPhoneInfoApi>(
        `http://apilayer.net/api/validate?access_key=6fe8930860effc82dd2b6db990edd7c8&number=${phoneNumber}`
      );

      const response = phoneInfo.data;
      const valid = response.valid;

      dispatch({
        type: PhoneNumberPanelActionTypes.SET_PHONE_NUMBER_VALID,
        payload: valid,
      });
    } catch (e) {
      console.log(e);
    }
  };
