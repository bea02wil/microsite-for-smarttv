import * as PhoneNumberPanelActions from "./phoneNumberPanelActions";

const Actions = {
  ...PhoneNumberPanelActions,
};

export default Actions;
