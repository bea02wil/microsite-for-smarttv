import React, { FC, useRef } from "react";
import classes from "./NumberEntryScreen.module.scss";
import PhoneNumberPanel from "../../components/phoneNumberPanel/PhoneNumberPanel";
import NumberEntryFinalDecision from "../../components/numberEntryFinalDecision/NumberEntryFinalDecision";
import { useActions } from "../../hooks/useActions";
import { PhoneKeyValue } from "../../types/phoneKeyItemTypes";

interface INumberEntryScreenProps {
  backgroundImage: string;
  productName: string;
  productLogo: string;
}

const NumberEntryScreen: FC<INumberEntryScreenProps> = ({
  backgroundImage,
  productName,
  productLogo,
}) => {
  const { setKeyFocus } = useActions();

  const componentnRef = useRef<HTMLDivElement>(null);

  const onKeyDownHandler = (e: React.KeyboardEvent<HTMLDivElement>) => {
    const arrowKeys: string[] = [
      "ArrowLeft",
      "ArrowRight",
      "ArrowUp",
      "ArrowDown",
    ];

    if (arrowKeys.includes(e.key)) {
      if (componentnRef.current === document.activeElement) {
        setKeyFocus(PhoneKeyValue.Five);
      }
    }
  };

  return (
    <div
      id="numberEntryScreen"
      className={classes.numberEntryScreen}
      style={{ background: `url(${backgroundImage})` }}
      onKeyDown={onKeyDownHandler}
      ref={componentnRef}
      tabIndex={-1}
    >
      <section className={classes.sectionLeft}>
        <PhoneNumberPanel productName={productName} ref={componentnRef} />
      </section>
      <section className={classes.sectionRight}>
        <NumberEntryFinalDecision logo={productLogo} ref={componentnRef} />
      </section>
    </div>
  );
};

export default NumberEntryScreen;
