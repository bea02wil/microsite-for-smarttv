import classes from "./ProductInfoScreen.module.scss";
import ImageCarousel from "../../components/ImageCarousel/ImageCarousel";
import AboutProduct from "../../components/aboutProduct/AboutProduct";
import { IProductInfoProps } from "../../types/aboutProductTypes";
import { ICarouselImageData } from "../../types/imageCarouselTypes";
import { FC, useRef } from "react";

interface IProductInfoScreenProps {
  images: ICarouselImageData[];
  aboutProductData: IProductInfoProps;
  productlogo: string;
}

const ProductInfoScreen: FC<IProductInfoScreenProps> = ({
  images,
  aboutProductData,
}) => {
  const componentnRef = useRef<HTMLDivElement>(null);

  const onClickHandler = () => {
    const imageCarouselContainer = componentnRef.current?.querySelector(
      "#imageCarouselContainer"
    ) as HTMLElement;
    imageCarouselContainer?.focus();
  };

  return (
    <div
      className={classes.productInfoScreen}
      onClick={onClickHandler}
      ref={componentnRef}
    >
      <section className={classes.sectionLeft}>
        <ImageCarousel images={images} />
      </section>
      <section className={classes.sectionRight}>
        <AboutProduct {...aboutProductData} />
      </section>
    </div>
  );
};

export default ProductInfoScreen;
