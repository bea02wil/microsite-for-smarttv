import { FC } from "react";
import PromoVideo from "../../components/promoVideo/PromoVideo";
import Banner from "../../components/banner/Banner";

interface IPromoVideoWithBannerProps {
  videoTitle: string;
  src: string;
  bannerImg: string;
}

const PromoVideoWithBannerScreen: FC<IPromoVideoWithBannerProps> = ({
  videoTitle,
  src,
  bannerImg,
}) => {
  return (
    <div>
      <PromoVideo width="1280" height="720" title={videoTitle} src={src} />
      <Banner
        acceptTitle="жми"
        buttonKeyName="ок"
        bodyText="получить персональное предложение"
        bodyImg={bannerImg}
        appearanceTime={5}
      />
    </div>
  );
};

export default PromoVideoWithBannerScreen;
