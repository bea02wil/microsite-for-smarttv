import { IProductCompany } from "../types/aboutProductTypes";
import Mazda6 from "../data/img/mazda6.png";
import MazdaLogo from "../data/img/MazdaLogo.png";
import MazdaCx9 from "../data/img/MazdaCx9.png";
import MazdaNumberEntryScreenBG from "./img/MazdaNumberEntryScreenBG.jpg";
import HondaNumberEntryScreenBG from "./img/HondaNumberEntryScreenBG.png";
import CircularView from "./img/CircularView.png";
import CruiseControl from "./img/CruiseControl.png";
import AdaptiveLight from "./img/AdaptiveLight.png";
import MonitoringDeadZone from "./img/MonitoringDeadZone.png";
import StopSystem from "./img/StopSystem.png";
import EmergencyResponseSystem from "./img/EmergencyResponseSystem.png";
import LineExitWarningSystem from "./img/LineExitWarningSystem.png";
import TrafficSignRecognitionSystem from "./img/TrafficSignRecognitionSystem.png";
import HondaCrV from "./img/HondaCrV.png";
import HondaLogo from "./img/HondaLogo.png";
import WirelessCharger from "./img/WirelessCharger.png";
import CleverKey from "./img/CleverKey.png";
import LaneWatch from "./img/LaneWatch.png";
import MultimediaSystem from "./img/MultimediaSystem.png";
import FatigueControl from "./img/FatigueControl.png";
import HondaPilot from "./img/HondaPilot.png";
import NoiseReductionSystem from "./img/NoiseReductionSystem.png";
import AutomaticTrunk from "./img/AutomaticTrunk.png";
import Econ from "./img/Econ.png";

export const mazda: IProductCompany = {
  company: {
    brandName: "Mazda",
    name: "мазда карс",
    promoVideo: {
      title: "Mazda showreel",
      src: "https://player.vimeo.com/video/127007761?background=1&autopause=0#t=1m4s",
    },
    banner: {
      img: Mazda6,
    },
    backgroundImg: MazdaNumberEntryScreenBG,
    model: {
      firstModel: {
        productTitle: "mazda 6",
        logo: MazdaLogo,
        featuresTitle: "специальная серия*",
        features: [
          "пакет --Зимний--",
          "гарантия 4 года или 150 000 км",
          "парковочный подогреватель",
        ],
        productImage: Mazda6,
        productProgramDescription:
          "*Предложение ограничено. Программа действует " +
          "при приобретении автомобиля с 1 ноября по 30 декабря " +
          "2014 года и не суммируется с другими предложениями. " +
          "Полный перечень дополнительного оборудования, участвующуего " +
          "в предложении и подробную информацию уточняйте у официальных " +
          "дилеров ООО --Мазда Карс-- на территории Российской Федерации." +
          "Указанный гарантийный срок распространяется на автомобили Mazda 6 " +
          "2016 модельного года, приобретенные у официальных дилеров ООО " +
          "--Мазда Карс-- и составляет 4 года или 150 000 км пробега (в зависимости " +
          "от того, что наступит ранее). Не является публичной офертой.",
        carouselImages: [
          {
            title: "circular viewing system",
            description: "парковка станет безопаснее",
            image: CircularView,
          },
          {
            title: "cruise control",
            description: "безопасная дистанция",
            image: CruiseControl,
          },
          {
            title: "adaptive light",
            description: "больше не слепит водителей",
            image: AdaptiveLight,
          },
          {
            title: "monitoring dead zone",
            description: "радар ближайших машин",
            image: MonitoringDeadZone,
          },
          {
            title: "stop system",
            description: "контроль расстояния",
            image: StopSystem,
          },
        ],
      },
      secondModel: {
        productTitle: "mazda CX-9",
        logo: MazdaLogo,
        featuresTitle: "пакет --зимний--",
        features: [
          "комплект зимних колес в сборе",
          "гарантия 4 года или 200 000 км*",
          "парковочный подогреватель",
        ],
        productImage: MazdaCx9,
        productProgramDescription:
          "*Предложение ограничено. Программа действует " +
          "при приобретении автомобиля с 1 ноября по 30 декабря " +
          "2014 года и не суммируется с другими предложениями. " +
          "Полный перечень дополнительного оборудования, участвующуего " +
          "в предложении и подробную информацию уточняйте у официальных " +
          "дилеров ООО --Мазда Карс-- на территории Российской Федерации." +
          "Не является публичной офертой.",
        carouselImages: [
          {
            title: "era-glonass",
            description: "автоматический вызов скорой помощи",
            image: EmergencyResponseSystem,
          },
          {
            title: "lane-keep assist system",
            description: "предупреждает об изменении полосы движения",
            image: LineExitWarningSystem,
          },
          {
            title: "traffic sign recognition",
            description: "распознает скрытые от водителя знаки",
            image: TrafficSignRecognitionSystem,
          },
        ],
      },
    },
  },
};

export const honda: IProductCompany = {
  company: {
    brandName: "Honda",
    name: "хонда карс",
    backgroundImg: HondaNumberEntryScreenBG,
    promoVideo: {
      title: "Honda showreel",
      src: "https://player.vimeo.com/video/349441479?background=1&autopause=0",
    },
    banner: {
      img: HondaCrV,
    },
    model: {
      firstModel: {
        productTitle: "honda cr-v",
        logo: HondaLogo,
        featuresTitle: "специальная серия*",
        features: [
          "пакет --Зимний--",
          "гарантия 4 года или 200 000 км",
          "парковочный подогреватель",
        ],
        productImage: HondaCrV,
        productProgramDescription:
          "*Предложение ограничено. Программа действует " +
          "при приобретении автомобиля с 1 сентября по 30 ноября " +
          "2015 года и не суммируется с другими предложениями. " +
          "Полный перечень дополнительного оборудования, участвующуего " +
          "в предложении и подробную информацию уточняйте у официальных " +
          "дилеров ООО --Хонда Карс-- на территории Российской Федерации." +
          "Указанный гарантийный срок распространяется на автомобили Honda Cr-V " +
          "2017 модельного года, приобретенные у официальных дилеров ООО " +
          "--Хонда Карс-- и составляет 4 года или 200 000 км пробега (в зависимости " +
          "от того, что наступит ранее). Не является публичной офертой.",
        carouselImages: [
          {
            title: "wireless charger",
            description: "всегда оставайтесь на связи",
            image: WirelessCharger,
          },
          {
            title: "clever key",
            description: "безключевой доступ в автомобиль",
            image: CleverKey,
          },
          {
            title: "lanewatch",
            description: "контроль слепой зоны",
            image: LaneWatch,
          },
          {
            title: "honda connect",
            description: "Многофункциональная медиа система",
            image: MultimediaSystem,
          },
          {
            title: "fatigue control",
            description: "Слежение за вашей усталостью",
            image: FatigueControl,
          },
        ],
      },
      secondModel: {
        productTitle: "honda pilot",
        logo: HondaLogo,
        featuresTitle: "пакет --зимний--",
        features: [
          "парковочный подогреватель",
          "комплект зимних колес в сборе",
          "гарантия 3 года или 160 000 км",
        ],
        productImage: HondaPilot,
        productProgramDescription:
          "*Предложение ограничено. Программа действует " +
          "при приобретении автомобиля с 1 октября по 30 декабря " +
          "2017 года и не суммируется с другими предложениями. " +
          "Полный перечень дополнительного оборудования, участвующуего " +
          "в предложении и подробную информацию уточняйте у официальных " +
          "дилеров ООО --Хонда Карс-- на территории Российской Федерации." +
          "Не является публичной офертой.",
        carouselImages: [
          {
            title: "active noise cancelation",
            description: "активное шумоподавление",
            image: NoiseReductionSystem,
          },
          {
            title: "automatic trunk",
            description: "можно открыть багажник с пакетами в руках",
            image: AutomaticTrunk,
          },
          {
            title: "econ",
            description: "экономный расход топлива",
            image: Econ,
          },
        ],
      },
    },
  },
};
