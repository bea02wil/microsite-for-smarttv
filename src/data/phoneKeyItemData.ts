import {
  IPhoneKeyItemProps,
  PhoneKeyValue,
  PhoneKeyItemClickAction,
} from "../types/phoneKeyItemTypes";
import classes from "../components/phoneKeyItem/PhoneKeyItem.module.scss";
import parse from "html-react-parser";

export const phoneKeyItemData: IPhoneKeyItemProps[] = [
  {
    xIndex: 0,
    yIndex: 0,
    keyValue: PhoneKeyValue.One,
  },
  {
    xIndex: 0,
    yIndex: 1,
    keyValue: PhoneKeyValue.Two,
  },
  {
    xIndex: 0,
    yIndex: 2,
    keyValue: PhoneKeyValue.Three,
  },
  {
    xIndex: 1,
    yIndex: 0,
    keyValue: PhoneKeyValue.Four,
  },
  {
    xIndex: 1,
    yIndex: 1,
    keyValue: PhoneKeyValue.Five,
  },
  {
    xIndex: 1,
    yIndex: 2,
    keyValue: PhoneKeyValue.Six,
  },
  {
    xIndex: 2,
    yIndex: 0,
    keyValue: PhoneKeyValue.Seven,
  },
  {
    xIndex: 2,
    yIndex: 1,
    keyValue: PhoneKeyValue.Eight,
  },
  {
    xIndex: 2,
    yIndex: 2,
    keyValue: PhoneKeyValue.Nine,
  },
  {
    keyValue: PhoneKeyValue.None,
    itemClass: classes.phoneKeyItemNotVisible,
  },
  {
    xIndex: 3,
    yIndex: 1,
    keyValue: PhoneKeyValue.Zero,
  },
  {
    xIndex: 3,
    yIndex: 2,
    keyValue: parse(`<i class="fas fa-backspace"></i>`),
    clickAction: PhoneKeyItemClickAction.EraseDigit,
  },
];
