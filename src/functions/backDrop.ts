interface IBackdrop {
  add: (backdropClass: string) => void;
  remove: (backdropClass: string) => void;
}

const getMicroSiteComponent = (): HTMLElement => {
  const microSiteComponent = document.getElementById("microSite");

  return microSiteComponent!;
};

const add = (backdropClass: string): void => {
  getMicroSiteComponent()?.classList.add(backdropClass);
};

const remove = (backdropClass: string) => {
  getMicroSiteComponent()?.classList.remove(backdropClass);
};

export const backdrop: IBackdrop = {
  add,
  remove,
};
