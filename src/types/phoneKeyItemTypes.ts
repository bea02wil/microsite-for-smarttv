import React from "react";

type keyValueType = string | React.ReactElement | React.ReactElement[];

export enum PhoneKeyItemClickAction {
  SetDigit = "SetPhoneNumberDigit",
  EraseDigit = "EraseDigit",
}

export enum PhoneKeyValue {
  None = "none",
  Zero = "0",
  One = "1",
  Two = "2",
  Three = "3",
  Four = "4",
  Five = "5",
  Six = "6",
  Seven = "7",
  Eight = "8",
  Nine = "9",
}

export interface IPhoneKeyItemProps {
  xIndex?: number;
  yIndex?: number;
  keyValue: keyValueType;
  itemClass?: string;
  clickAction?: string;
}
