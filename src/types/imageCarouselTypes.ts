export interface ICarouselImageData {
  title: string;
  description: string;
  image: string;
}
