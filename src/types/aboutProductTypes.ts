import { ICarouselImageData } from "./imageCarouselTypes";

export interface IProductInfoProps {
  productTitle: string;
  logo: string;
  featuresTitle: string;
  features: string[];
  productImage: string;
  productProgramDescription: string;
  carouselImages?: ICarouselImageData[];
}

interface IProductModel {
  firstModel: IProductInfoProps;
  secondModel: IProductInfoProps;
}

interface IProductPromoVideo {
  title: string;
  src: string;
}

interface IProductBanner {
  img: string;
}

interface IProductInfo {
  brandName: string;
  name: string;
  backgroundImg: string;
  promoVideo: IProductPromoVideo;
  banner: IProductBanner;
  model: IProductModel;
}

export interface IProductCompany {
  company: IProductInfo;
}
