export enum Path {
  Home = "/",
  ProductInfoScreen = "/product-info-screen",
  ProductInfoScreenExit = "/product-info-screen/exit",
  NumberEntryScreen = "/number-entry-screen",
  NumberEntryScreenContactAlert = "/number-entry-screen/contact-alert",
  NumberEntryScreenExit = "/number-entry-screen/exit",
  ProductInfoScreenAlternativeProduct = "/product-info-screen/alternative-product",
  ProductInfoScreenAlternativeProductExit = "/product-info-screen/alternative-product/exit",
  Exit = "/exit",
}
