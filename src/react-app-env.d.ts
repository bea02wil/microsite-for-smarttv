/// <reference types="react-scripts" />

//Не получается сделать .env.local в TS. Возвращает undefined
declare namespace NodeJS {
  export interface ProcessEnv {
    REACT_APP_PHONE_VALIDATE_API_KEY: string;
    REACT_APP_API_URL: string;
  }
}
