import { mazda, honda } from "./data/aboutProductData";
import "./App.module.scss";
import MicroSite from "./components/microSite/MicroSite";

const App = () => {
  //Можешь дать мне honda
  return <MicroSite product={mazda} />;
};

export default App;
